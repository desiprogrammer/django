from django.shortcuts import render, redirect
# Create your views here.
from django.core.files.storage import FileSystemStorage
from updateapp.models import simpleprofile
import os
from django.conf import settings
from django.conf import settings


# simple route to get all data 
def home(request):
    alldata = simpleprofile.objects.all()
    return render(request, 'home.html', {'data': alldata})


def upload(request):
    if request.method == 'POST' and request.FILES['file']:
        myfile = request.FILES['file']
        fs = FileSystemStorage()
        # simply using name provided as filename 
        # but removing all spaces
        # thats mandatory 
        filename = myfile.name.replace(" ", "")
        filename = fs.save(filename, myfile)
        url = fs.url(filename)
        new_data = simpleprofile(name=request.POST['name'], fileurl=url)
        new_data.save()
        return redirect('/home/')
    else:
        return render(request, 'home.html')


def updater(request):
    alldata = simpleprofile.objects.all()
    return render(request, 'update.html', {'data': alldata})


def updatefile(request):
    if request.method == 'POST' and request.FILES['file']:
        data = simpleprofile.objects.get(name=request.POST['select'])
        # delete old file
        os.remove(data.fileurl[1:])
        myfile = request.FILES['file']
        fs = FileSystemStorage()
        filename = myfile.name.replace(" ", "")
        filename = fs.save(filename, myfile)
        url = fs.url(filename)
        # update new data 
        data.fileurl = url
        data.save()
        return redirect('/home/')